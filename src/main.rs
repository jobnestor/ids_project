use plotlib::page::Page;
use plotlib::repr::Plot;
use plotlib::view::ContinuousView;
use plotlib::style::{PointMarker, PointStyle};
use std::io;
use std::fs;

fn generate_data_set()    {

}

fn expectation_minimization()   {
    assign_points();
    recompute_clusters();
}

fn show_dataset_scattergraph(data: &Vec<(f64, f64)>)  {
    let data_val = data.clone();
    let s1: Plot = Plot::new(data_val).point_style(
        PointStyle::new()
            .marker(PointMarker::Square) // setting the marker to be a square
            .colour("#DD3355"),
    );
}

fn intialise_clusters() -> i32 {
    let k = choose_k();
    compute_centroid();
    return k;
}

fn choose_k() -> i32   {
    let mut k = String::new();
    println!("\n\n Please choose a value for k:\t
    ");
    io::stdin()
        .read_line(&mut k)
        .expect("Failed to read k");
        let k = k.trim().parse::<i32>().expect("invalid input");
    return k;
}

fn assign_points()  {

}

fn compute_centroid()   {
    recompute_clusters();
}

fn recompute_clusters() {
    
}

fn read_from_file() -> String {
    let mut file = String::new();
    let name: &'static str = "./data.txt";
    file = fs::read_to_string(name)
    .expect("Something went wrong reading the file");
    return file;
}

fn main() {
let mut raw_data = String::new();
raw_data = read_from_file();
let mut data: Vec<(f64, f64)> = vec![];
let mut datatest = raw_data.as_bytes().to_vec();
println!("{:?}", datatest);

    println!("Generating a synthetic dataset...");
    generate_data_set();
    println!("Choosing K...");
    let k = intialise_clusters();
    println!("Showing initial dataset...");
    // show_dataset_scattergraph(&data);
    println!("Computing k-means on given dataset...");
    println!("Performing expectation-maximization...");
    expectation_minimization();
    println!("Showing clustered dataset...");
    // show_dataset_scattergraph(&data);
}