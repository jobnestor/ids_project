This is a project that is done to fulfill the requirements of a course on intrusion detection in physical and virtual networks. The aim is to implement the k-means cluster search algorithm, as well as writing about and implementing the improved k-means++ algorithm. This can be done in any programming language, so I am seizing this opportunity to learn some Rust.

If I have time, I might try to port this into different languages, such as Lisp, C, C++, and others.

Intially, the k-means clustering problem algorithm chosen is the Lloyd-Max algorithm (In a 2D space, not a 3D Integration space).

The second iteration of this program will seek to implement the k-means++ algorith, which has an improved intitialization stage, among others.